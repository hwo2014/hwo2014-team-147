package com.houston.slot_car.racer;

import com.google.gson.internal.LinkedTreeMap;
import com.houston.slot_car.Car;
import com.houston.slot_car.Lane;
import com.houston.slot_car.TrackPiece;
import com.houston.slot_car.message.SendMsg;
import com.houston.slot_car.message.SwitchLane;
import com.houston.slot_car.message.from_server.CarPositionData;
import com.houston.slot_car.message.from_server.CarPositions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public abstract class Racer {

    public static final BigDecimal SPEED_MULTIPLIER_WITH_NO_THROTTLE = new BigDecimal(0.98);

    private Car myCar;
    private List<TrackPiece> track;
    private List<Lane> lanes;

    private CarPositionData previousCarPositionData = null;

    private BigDecimal prevSpeed = BigDecimal.ZERO;
    private double prevThrottle = 0;
    private BigDecimal prevAngleChange = BigDecimal.ZERO;

    public abstract SendMsg getAction(CarPositions carPositions);

    public Car getMyCar() {
        return myCar;
    }

    public void setMyCar(Car myCar) {
        this.myCar = myCar;
    }

    public List<TrackPiece> getTrack() {
        return track;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public int getPrevPiece() {
        return previousCarPositionData == null ? -1 : previousCarPositionData.getPiecePosition().getPieceIndex();
    }

    public BigDecimal getPreviousAngle() {
        return previousCarPositionData == null ? BigDecimal.ZERO : previousCarPositionData.getAngle();
    }

    public double getPrevThrottle() {
        return prevThrottle;
    }

    public void setPrevThrottle(double prevThrottle) {
        this.prevThrottle = prevThrottle;
    }

    public BigDecimal getPrevInpieceDistance() {
        return previousCarPositionData == null ? BigDecimal.ZERO : previousCarPositionData.getPiecePosition().getInPieceDistance();
    }

    public BigDecimal getPrevSpeed() {
        return prevSpeed;
    }

    public void setPrevSpeed(BigDecimal prevSpeed) {
        this.prevSpeed = prevSpeed;
    }

    public void setPrevAngleChange(BigDecimal prevAngleChange) {
        this.prevAngleChange = prevAngleChange;
    }

    public BigDecimal getPrevAngleChange() {
        return prevAngleChange;
    }

    protected int getNextTrackPieceIndex(int currentTrackPieceIndex) {
        return (currentTrackPieceIndex + 1) % track.size();
    }

    protected TrackPiece getNextPiece(int currentPieceIndex) {
        return track.get(getNextTrackPieceIndex(currentPieceIndex));
    }

    public void buildTrack(ArrayList<LinkedTreeMap> pieces) {
        track = new ArrayList<TrackPiece>();
        for (LinkedTreeMap piece : pieces) {
            track.add(new TrackPiece(piece));
        }

        for (int i = 0; i < track.size(); ++i) {
            System.out.println(i + ": " + track.get(i));
        }
    }

    public void setLanes(ArrayList<LinkedTreeMap> laneList) {
        lanes = new ArrayList<Lane>();
        for (LinkedTreeMap lane : laneList) {
            lanes.add(
                    new Lane(
                            new Double(lane.get("index").toString()).intValue(),
                            new Double(lane.get("distanceFromCenter").toString()).intValue()));
        }
    }

    public BigDecimal getCurrentSpeed(CarPositionData carPositionData) {
        if (getPrevPiece() < 0) { return BigDecimal.ZERO; }
        BigDecimal speed;
        if (getPrevPiece() == carPositionData.getPiecePosition().getPieceIndex()) {
            speed = carPositionData.getPiecePosition().getInPieceDistance().subtract(getPrevInpieceDistance());
            speed = speed.multiply(new BigDecimal("0.1")).setScale(4, RoundingMode.HALF_UP);
        } else {
            Lane lane = lanes.get(carPositionData.getPiecePosition().getLaneDetails().getStartLaneIndex());
            TrackPiece previousTrackPiece = track.get(getPrevPiece());

            speed = new BigDecimal(getLengthForMyLaneInTrackPiece(previousTrackPiece, lane));
            speed = speed.subtract(getPrevInpieceDistance());
            speed = speed.add(carPositionData.getPiecePosition().getInPieceDistance());
            speed = speed.multiply(new BigDecimal("0.1")).setScale(4, RoundingMode.HALF_UP);
        }
        return speed;
    }

    public void setPreviousCarPositionData(CarPositionData previousCarPositionData) {
        this.previousCarPositionData = previousCarPositionData;
    }

    public void printCrashDetails() {
        System.out.println(
                String.format(
                        "lap: %d, prev: {piece: %d, distance inside piece: %d, speed: %s, angle: %s, throttle: %f}",
                        previousCarPositionData.getPiecePosition().getLap(),
                        previousCarPositionData.getPiecePosition().getPieceIndex(),
                        previousCarPositionData.getPiecePosition().getInPieceDistance().intValue(),
                        prevSpeed.toString(),
                        previousCarPositionData.getAngle().toString(),
                        prevThrottle));
    }

    protected boolean iShouldBeBreakingForCorner(int currentTrackPieceIndex, CarPositionData carPositionData, BigDecimal speed, Lane lane) {
        TrackPiece currentTrackPiece = track.get(currentTrackPieceIndex);
        TrackPiece nextTrackPiece = getNextPiece(currentTrackPieceIndex);
        int nextRadius = getRadiusForMyLane(nextTrackPiece, lane);

        BigDecimal inPieceDistance = carPositionData.getPiecePosition().getInPieceDistance();

        // B2 = B1 + 0.02 - (B1*0.02)

        if (nextRadius < 90) {
            if (inPieceDistance.doubleValue() > (currentTrackPiece.getLength() * 0.9)) {
                return true;
            }

            if (speed.doubleValue() < 0.5) {
                return false;
            }

            return true;
        } else if (nextRadius < 190) {
            Double ifIBrakeNow = calculateSpeedAtTheEndWithNoThrottle(inPieceDistance, speed, currentTrackPiece);

            List<TrackPiece> nextCorner = getNextCorner(currentTrackPieceIndex);
            if (nextCorner.size() < 2) {
                return false;
            } else if (nextCorner.size() < 3) {
                if (speed.doubleValue() > 0.7 && inPieceDistance.doubleValue() > (currentTrackPiece.getLength() * 0.8)) {
                    return true;
                }
                return false;
            }


            if (speed.doubleValue() < 0.7) {
                return false;
            }

            if (haveDifferentSigns(nextTrackPiece.getAngle(), getPreviousAngle().intValue())) { return false; }

            if (speed.doubleValue() > 0.75) {
                return true;
            } else if (inPieceDistance.intValue() > (currentTrackPiece.getLength() * 0.5)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    private List<TrackPiece> getNextCorner(int currentTrackPieceIndex) {
        List<TrackPiece> corner = new ArrayList<TrackPiece>();
        int nextTrackPieceId = getNextTrackPieceIndex(currentTrackPieceIndex);
        TrackPiece trackPiece = track.get(nextTrackPieceId);
        while (!trackPiece.isStraight()) {
            corner.add(trackPiece);
            nextTrackPieceId = getNextTrackPieceIndex(nextTrackPieceId);
            trackPiece = track.get(nextTrackPieceId);
        }
        return corner;
    }

    private Double calculateSpeedAtTheEndWithNoThrottle(BigDecimal inPieceDistance, BigDecimal speed, TrackPiece currentTrackPiece) {

        // System.out.println("    inPieceDistance: " + inPieceDistance + ", speed: " + speed + " throttle: " + getPrevThrottle() + ", length: " + currentTrackPiece.getLength());
        // TODO

        return 0d;

    }

    protected int getRadiusForMyLane(TrackPiece trackPiece, Lane lane) {
        if (trackPiece.getRadius() == 0) { return 0; }
        if (trackPiece.getAngle() < 0) {
            return trackPiece.getRadius() + lane.getDistanceFromCenter();
        } else {
            return trackPiece.getRadius() - lane.getDistanceFromCenter();
        }
    }

    protected double getLengthForMyLaneInTrackPiece(TrackPiece trackPiece, Lane lane) {
        if (trackPiece.isStraight()) { return trackPiece.getLength(); }
        int radius = getRadiusForMyLane(trackPiece, lane);
        Double radians = Math.toRadians(new Double(trackPiece.getAngle()));
        return Math.abs(radians.doubleValue() * radius);
    }

    protected Double calculateSectionLength(List<TrackPiece> section, Lane lane) {
        Double length = 0d;
        for (int i = 0; i < section.size(); ++i) {
            TrackPiece trackPiece = section.get(i);
            Double pieceLength;
            if (trackPiece.isStraight()) {
                pieceLength = trackPiece.getLength();
            } else {
                pieceLength = getLengthForMyLaneInTrackPiece(trackPiece, lane);
            }

            if (i == 0 || i == (section.size() - 1)) {
                pieceLength = pieceLength / 2;
            }

            length += pieceLength;
        }
        return length;
    }

    protected SwitchLane getSwitchIfINeedOne(Lane currentLane, int currentPieceIndex) {
        List<TrackPiece> section = getTrackUntilNextSwitch(currentPieceIndex);

        Lane shortest = null;
        Double shortestLength = null;
        Double currentLength = null;

        for (Lane l : getLanes()) {
            Double length = calculateSectionLength(section, l);
            if (shortestLength == null || shortestLength > length) {
                shortest = l;
                shortestLength = length;
            }
            if (l.getIndex() == currentLane.getIndex()) {
                currentLength = length;
            }
        }

        if (shortestLength < currentLength) {
            if (shortest.getDistanceFromCenter() > currentLane.getDistanceFromCenter()) {
                return new SwitchLane(SwitchLane.RIGHT);
            } else {
                return new SwitchLane(SwitchLane.LEFT);
            }
        }
        return null;
    }

    protected List<TrackPiece> getTrackUntilNextSwitch(final int piece) {
        List<TrackPiece> pieces = new ArrayList<TrackPiece>();

        int firstPieceIndex = getNextTrackPieceIndex(piece);
        while(!track.get(firstPieceIndex).canSwitchLanes()) {
            firstPieceIndex = getNextTrackPieceIndex(firstPieceIndex);
            if (firstPieceIndex == piece) { return pieces; } // no crossings
        }

        pieces.add(track.get(firstPieceIndex)); // first switch piece
        int nextPieceIndex = getNextTrackPieceIndex(firstPieceIndex);

        TrackPiece next;
        while(!(next = track.get(nextPieceIndex)).canSwitchLanes()) {
            pieces.add(next);
            nextPieceIndex = getNextTrackPieceIndex(nextPieceIndex);
        }

        pieces.add(track.get(nextPieceIndex));  // including the second switch piece
        return pieces;
    }

    protected BigDecimal getAngleChange(BigDecimal newAngle, BigDecimal prevAngle, int turnAngle) {
        if (turnAngle > 0) {
            return newAngle.subtract(prevAngle);
        } else {
            return prevAngle.subtract(newAngle);
        }
    }


    protected boolean haveDifferentSigns(int one, int two) {
        if (one > 0 && two < 0) { return true; }
        if (one < 0 && two > 0) { return true; }
        return false;
    }
}
