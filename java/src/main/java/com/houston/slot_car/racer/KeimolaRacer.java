package com.houston.slot_car.racer;

import com.houston.slot_car.Lane;
import com.houston.slot_car.TrackPiece;
import com.houston.slot_car.message.SendMsg;
import com.houston.slot_car.message.SwitchLane;
import com.houston.slot_car.message.Throttle;
import com.houston.slot_car.message.from_server.CarPositionData;
import com.houston.slot_car.message.from_server.CarPositions;

import java.math.BigDecimal;

public class KeimolaRacer extends Racer {

    private static final double MAX_SPEED_FOR_INNER_LANE = 0.65;
    private static final double MAX_SPEED_FOR_OUTER_LANE = 0.6568;

    @Override
    public SendMsg getAction(CarPositions carPositions) {

        final CarPositionData carPositionData = carPositions.getMyPositionData(getMyCar());

        final int currentPieceIndex = carPositionData.getPiecePosition().getPieceIndex();
        final TrackPiece currentPiece = getTrack().get(currentPieceIndex);
        final TrackPiece nextPiece = getNextPiece(currentPieceIndex);

        final Lane lane = getLanes().get(carPositionData.getPiecePosition().getLaneDetails().getStartLaneIndex());
        final BigDecimal speed = getCurrentSpeed(carPositionData);
        final int angle = carPositionData.getAngle().intValue();
        final BigDecimal angleChange = carPositionData.getAngle().abs().subtract(getPreviousAngle().abs());


        // LOG

        if (currentPiece.getAngle() == 22 || currentPiece.getAngle() == -22) {
            System.out.println(String.format("         catch 22 - ANGLE %d SPEED %f THROTTLE %f", carPositionData.getAngle().intValue(), speed, getPrevThrottle()));
        }

        if (getPrevPiece() != currentPieceIndex) {

            if (getPrevPiece() != -1 && currentPiece.isStraight() && !getTrack().get(getPrevPiece()).isStraight() && Math.abs(angle) < 50) {
                System.out.println("LOOK! I can do better here!");
            }


            if (!currentPiece.isStraight()) {
                int radius = getRadiusForMyLane(currentPiece, lane);
                System.out.println(
                        String.format(
                                "Entering corner %d with radius: %d, angle: %d, length: %f in lane: %d, while my speed: %f, and angle: %d, angle change: %f, throttle: %f",
                                currentPieceIndex,
                                radius,
                                currentPiece.getAngle(),
                                getLengthForMyLaneInTrackPiece(currentPiece, lane),
                                lane.getIndex(),
                                speed,
                                angle,
                                angleChange,
                                getPrevThrottle()));
            } else {
                System.out.println(
                        String.format(
                                "Entering straight %d with length: %f in lane: %d. My speed: %f, angle: %d, angle change: %f, throttle: %f",
                                currentPieceIndex,
                                currentPiece.getLength(),
                                lane.getIndex(),
                                speed,
                                angle,
                                angleChange,
                                getPrevThrottle()));
            }
        }

        if (carPositionData.getAngle().abs().intValue() > 50) {
            System.out.println(String.format("  ANGLE %d ANGLE CHANGE %f SPEED %f THROTTLE %f", carPositionData.getAngle().intValue(), angleChange, speed, getPrevThrottle()));
        }

        // LOG OUT


        if (getPrevPiece() != currentPieceIndex && nextPiece.canSwitchLanes()) {
            SwitchLane switchLane = getSwitchIfINeedOne(lane, currentPieceIndex);
            if (switchLane != null) {
                setPreviousCarPositionData(carPositionData);
                setPrevSpeed(speed);
                setPrevThrottle(new Double(getPrevThrottle()));
                return switchLane;
            }
        }


        Throttle throttle;
        if (speed.doubleValue() < 0.2) {
            throttle = new Throttle(1.0);
        } else if (currentPiece.isStraight()) {
            throttle = handleStraightLine(currentPieceIndex, carPositionData, speed, currentPiece, nextPiece, lane);
        } else {
            int radius = getRadiusForMyLane(currentPiece, lane);
            Double length = getLengthForMyLaneInTrackPiece(currentPiece, lane);

            if (radius < 50) {
                throttle = handleRadFortyCorner(speed, length, carPositionData, currentPieceIndex);
            } else if (radius < 90) {
                throttle = handleRadFiftyCorner(speed, length, currentPiece, nextPiece, carPositionData);
            } else if (radius < 190) {
                throttle = handleRadHundredCorner(speed, length, carPositionData, currentPiece, nextPiece);
            } else {
                throttle = new Throttle(1.0);
            }
        }

        if (currentPieceIndex == 0 || currentPieceIndex == 1 || currentPieceIndex == 13 || currentPieceIndex == 27 || currentPieceIndex == 36) {
            System.out.println(String.format(" throttle: %s, speed: %f, angle: %d, distance in piece: %f", throttle.msgData().toString(), speed, angle, carPositionData.getPiecePosition().getInPieceDistance()));
        }


        setPreviousCarPositionData(carPositionData);
        setPrevSpeed(speed);
        setPrevThrottle(new Double(throttle.msgData().toString()));

        return throttle;
    }

    private Throttle handleStraightLine(int currentPieceIndex, CarPositionData carPositionData, BigDecimal speed, TrackPiece currentPiece, TrackPiece nextPiece, Lane lane) {
        if (nextPiece.isStraight()) {
            int nextPieceIndex = getNextTrackPieceIndex(currentPieceIndex);
            int afterNext = getNextTrackPieceIndex(nextPieceIndex);

            if (getTrack().get(afterNext).getRadius() == 50 &&
                    speed.doubleValue() > 0.8 &&
                    carPositionData.getPiecePosition().getInPieceDistance().intValue() > (int)(currentPiece.getLength() * 0.5)) {
                return new Throttle(0);
            } else {
                return new Throttle(1.0);
            }
        } else {
            if (iShouldBeBreakingForCorner(currentPieceIndex, carPositionData, speed, lane)) {
                return new Throttle(0);
            } else {
                return new Throttle(1.0);
            }
        }
    }

    private Throttle handleRadFortyCorner(BigDecimal speed, Double cornerLength, CarPositionData carPositionData, int currentPieceIndex) {

        int angleAbs = carPositionData.getAngle().abs().intValue();
        BigDecimal angleChange = getAngleChange(carPositionData.getAngle(), getPreviousAngle(), getTrack().get(currentPieceIndex).getAngle());
        TrackPiece nextTrackPiece = getNextPiece(currentPieceIndex);


        System.out.println(
                String.format(
                        "      throttle: %f, speed: %f, angle: %d, angle change: %f, distance %d/%d",
                        getPrevThrottle(),
                        speed,
                        carPositionData.getAngle().intValue(),
                        angleChange,
                        carPositionData.getPiecePosition().getInPieceDistance().intValue(),
                        cornerLength.intValue()));

        if (nextTrackPiece.isStraight() || nextTrackPiece.getRadius() > 100) {
            if (angleAbs < 50) {
                return new Throttle(1.0);
            } else {
                return new Throttle(0);
            }
        }

        if (nextTrackPiece.getRadius() == 100) {
            return new Throttle(0.45);
        }

        if (speed.doubleValue() > 0.58) {
            System.out.println("  SPEED IS TOO DAMN HIGH! (over 0,58)");
            return new Throttle(0);
        }

        if (angleChange.intValue() < 3) {
            return new Throttle(0.36);
        }

        return new Throttle(0);

    }

    private Throttle handleRadFiftyCorner(BigDecimal speed, Double cornerLength, TrackPiece currentTrackPiece, TrackPiece nextTrackPiece, CarPositionData carPositionData) {
        int angleAbs = carPositionData.getAngle().abs().intValue();
        int previousAngleAbs = getPreviousAngle().abs().intValue();
        int angleChange = getAngleChange(carPositionData.getAngle(), getPreviousAngle(), currentTrackPiece.getAngle()).intValue();


        System.out.println(
                String.format(
                        "  distance in piece: %d/%d, speed: %f, angle : %d angle change: %d, previous throttle: %f",
                        carPositionData.getPiecePosition().getInPieceDistance().intValue(),
                        cornerLength.intValue(),
                        speed,
                        carPositionData.getAngle().intValue(),
                        angleChange,
                        getPrevThrottle()));

        if (angleAbs > 55) {
            return new Throttle(0);
        }

        if (angleAbs > 50) {
            if (angleChange > 0) {
                return new Throttle(0);
            }
            return new Throttle(0.15);
        }

        if (angleAbs > 45 && angleChange > 2) {
            return new Throttle(0.0005);
        }

        if (
                (currentTrackPiece.getAngle() > 0 && carPositionData.getAngle().intValue() < 0) ||
                (currentTrackPiece.getAngle() < 0 && carPositionData.getAngle().intValue() > 0)) {
            if (speed.doubleValue() > 0.5) {
                if (angleChange > 4) {
                    return new Throttle(0.45);
                } else {
                    return new Throttle(0.500001);
                }
            } else {
                return new Throttle(Math.max(0.551, Math.min(getPrevThrottle() + 0.05, 0.7)));
            }
        }

        if (
                (nextTrackPiece.isStraight() || Math.abs(nextTrackPiece.getAngle()) == 22) &&
                (angleAbs < 30)) {
            if (angleChange > 3) {
                return new Throttle(0.0001);
            } else {
                if (carPositionData.getPiecePosition().getInPieceDistance().doubleValue() > (cornerLength * 0.5)) {
                    System.out.println("angleAbs: " + angleAbs + ", previousAngleAbs: " + previousAngleAbs);
                    return new Throttle(0.4);
                } else {
                    System.out.println("angleAbs: " + angleAbs + ", previousAngle: " + previousAngleAbs);
                    return new Throttle(0.0002);
                }
            }
        }

        if (
                (
                        (currentTrackPiece.getAngle() < 0 && carPositionData.getAngle().doubleValue() < 0) ||
                        (currentTrackPiece.getAngle() > 0 && carPositionData.getAngle().doubleValue() > 0)
                ) &&
                angleChange < 0) {




            return new Throttle(Math.max(0.6, Math.max(getPrevSpeed().doubleValue(), getPrevThrottle()) + 0.025));
        }

        if (angleAbs < 10 && angleChange < 3) {
            return new Throttle(0.6);
        }

        if (angleChange > 3) {
            return new Throttle(0.00003);
        }

        if (speed.doubleValue() > 0.55) {
            return new Throttle(0.2);
        }

        if (angleChange > 2) {
            if (angleAbs > 40) {
                return new Throttle(0.15);
            } else if (angleAbs > 30) {
                return new Throttle(0.2000007);
            } else {
                return new Throttle(0.3);
            }
        }

        return new Throttle(0.45);
    }



    private Throttle handleRadHundredCorner(BigDecimal speed, Double cornerLength, CarPositionData carPositionData, TrackPiece currentTrackPiece, TrackPiece nextTrackPiece) {

        int angle = carPositionData.getAngle().abs().intValue();
        BigDecimal angleChange = getAngleChange(carPositionData.getAngle(), getPreviousAngle(), currentTrackPiece.getAngle());

        if (angle > 56) {
            return new Throttle(0);
        }

        if (angle > 50 && angleChange.intValue() > 1) {
            return new Throttle(0);
        }

        if (Math.abs(currentTrackPiece.getAngle()) == 22 &&
            haveDifferentSigns(currentTrackPiece.getAngle(), carPositionData.getAngle().intValue())) {
            return new Throttle(1.0);
        }

        if ((nextTrackPiece.isStraight() || nextTrackPiece.getRadius() > 150) &&
            (carPositionData.getPiecePosition().getInPieceDistance().doubleValue() > (cornerLength / 2) || Math.abs(currentTrackPiece.getAngle()) <= 22) &&
            (carPositionData.getAngle().abs().intValue() < 45 || angleChange.intValue() < 1)) {
            return new Throttle(1.0);
        }

        if (speed.doubleValue() > MAX_SPEED_FOR_OUTER_LANE) {
            return new Throttle(0);
        }

        if (angleChange.intValue() > 3) {
            return new Throttle(0.5);
        }

        return new Throttle(MAX_SPEED_FOR_OUTER_LANE);
    }
}
