package com.houston.slot_car;

import com.google.gson.internal.LinkedTreeMap;

public class Car {
    private String name;
    private String color;

    public Car(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (!color.equals(car.color)) return false;
        if (!name.equals(car.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + color.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("a %s Car with name %s", color, name);
    }

    public static Car getCar(LinkedTreeMap<String, Object> data) {
        return new Car(data.get("name").toString(), data.get("color").toString());
    }
}
