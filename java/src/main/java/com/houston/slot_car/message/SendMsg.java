package com.houston.slot_car.message;

import com.google.gson.Gson;

public abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    public Object msgData() {
        return this;
    }

    public abstract String msgType();
}
