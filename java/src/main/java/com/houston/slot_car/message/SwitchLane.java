package com.houston.slot_car.message;

public class SwitchLane extends SendMsg {
    public static final String RIGHT = "Right";
    public static final String LEFT = "Left";

    private String data;

    public SwitchLane(String direction) {
        this.data = direction;
    }

    @Override
    public Object msgData() {
        return data;
    }

    @Override
    public String msgType() {
        return "switchLane";
    }
}