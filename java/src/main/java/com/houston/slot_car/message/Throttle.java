package com.houston.slot_car.message;

public class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    public Object msgData() {
        return value;
    }

    @Override
    public String msgType() {
        return "throttle";
    }
}