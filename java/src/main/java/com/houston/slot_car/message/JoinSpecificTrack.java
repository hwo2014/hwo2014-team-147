package com.houston.slot_car.message;

public class JoinSpecificTrack extends SendMsg {

    // {"msgType": "joinRace", "data": { "botId": { "name": "keke", "key": "IVMNERKWEW" }, "trackName": "germany", "carCount": 1 }}
    private BotId botId;
    private String trackName;
    private int carCount;

    public JoinSpecificTrack(String botName, String botKey, String trackName, int carCount) {
        this.botId = new BotId(botName, botKey);
        this.trackName = trackName;
        this.carCount = carCount;
    }

    @Override
    public String msgType() {
        return "joinRace";
    }

    private class BotId {
        String name;
        String key;

        public BotId(String name, String key) {
            this.name = name;
            this.key = key;
        }
    }
}


