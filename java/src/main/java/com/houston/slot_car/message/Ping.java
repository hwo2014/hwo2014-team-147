package com.houston.slot_car.message;

public class Ping extends SendMsg {
    @Override
    public String msgType() {
        return "ping";
    }
}