package com.houston.slot_car.message.from_server;

import com.houston.slot_car.Car;

import java.math.BigDecimal;

public class CarPositionData {
    private Car id;
    private BigDecimal angle;
    private PiecePosition piecePosition;

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public Car getCar() {
        return id;
    }

    public BigDecimal getAngle() {
        return angle;
    }

    @Override
    public String toString() {
        return String.format("Position data for Car %s: angle: %d, piecePosition: %s", id.toString(), angle.intValue(), piecePosition.toString());
    }

}