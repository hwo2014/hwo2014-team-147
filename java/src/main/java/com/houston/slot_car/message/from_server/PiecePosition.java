package com.houston.slot_car.message.from_server;

import java.math.BigDecimal;

public class PiecePosition {
    private Integer pieceIndex;
    private BigDecimal inPieceDistance;
    private LaneDetails lane;
    private Integer lap;

    public Integer getPieceIndex() {
        return pieceIndex;
    }

    public BigDecimal getInPieceDistance() {
        return inPieceDistance;
    }

    public Integer getLap() {
        return lap;
    }

    public LaneDetails getLaneDetails() {
        return lane;
    }

    @Override
    public String toString() {
        return String.format("lap: %d, piece index: %d piece distance: %d, lane: %d", lap, pieceIndex, inPieceDistance.intValue(), lane.getStartLaneIndex());
    }
}