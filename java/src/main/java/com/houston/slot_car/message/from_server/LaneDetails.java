package com.houston.slot_car.message.from_server;

public class LaneDetails {
    private Integer startLaneIndex;
    private Integer endLaneIndex;

    public Integer getStartLaneIndex() {
        return startLaneIndex;
    }

    public Integer getEndLaneIndex() {
        return endLaneIndex;
    }
}