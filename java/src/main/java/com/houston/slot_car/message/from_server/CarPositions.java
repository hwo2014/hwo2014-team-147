package com.houston.slot_car.message.from_server;

import com.houston.slot_car.Car;

import java.util.List;

public class CarPositions {
    // {
    //      "msgType":"carPositions",
    //      "data":[
    //          {
    //              "id":{"name":"lol","color":"red"},
    //              "angle":0.0,
    //              "piecePosition":{
    //                  "pieceIndex":0,
    //                  "inPieceDistance":0.39145280000000005,
    //                  "lane":{"startLaneIndex":0,"endLaneIndex":0},
    //                  "lap":0
    //              }
    //          }
    //      ],
    //      "gameId":"c3a6a6aa-3638-4448-90b9-5787ecb1d7e2",
    //      "gameTick":2
    // }

    private String msgType;
    private List<CarPositionData> data;
    private String gameId;
    private Integer gameTick;

    public CarPositionData getMyPositionData(Car car) {
        for (CarPositionData carPositionData : data) {
            if (carPositionData.getCar().equals(car)) {
                return carPositionData;
            }
        }
        return null;
    }





}
