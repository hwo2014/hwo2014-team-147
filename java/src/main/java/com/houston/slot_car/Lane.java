package com.houston.slot_car;

public class Lane {
    private int index;
    private int distanceFromCenter;

    public Lane(int index, int distanceFromCenter) {
        this.index = index;
        this.distanceFromCenter = distanceFromCenter;
    }

    public int getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return String.format("Lane no. %d with distance from center of %d", index, distanceFromCenter);
    }
}
