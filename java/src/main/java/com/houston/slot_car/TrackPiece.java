package com.houston.slot_car;

import java.util.Map;

public class TrackPiece {
    double length;
    int radius;
    int angle;
    boolean canSwitchLanes;

    public TrackPiece(Map map) {
        this.canSwitchLanes = map.get("switch") != null && map.get("switch").toString().equals("true");
        this.length = map.get("length") == null ? 0 : new Double(map.get("length").toString());
        this.radius = map.get("radius") == null ? 0 : new Double(map.get("radius").toString()).intValue();
        this.angle  = map.get("angle") == null  ? 0 : new Double(map.get("angle").toString()).intValue();
    }

    public boolean isStraight() {
        return this.angle == 0;
    }

    @Override
    public String toString() {
        if (length > 0) { return "straight piece with length of " + length + (canSwitchLanes ? " with a lane crossing" : ""); }
        else { return "bend with angle " + angle + " and radius of " + radius + (canSwitchLanes ? " also containing a lane crossing" : ""); }
    }

    public double getLength() {
        return length;
    }

    public int getAngle() {
        return angle;
    }

    public boolean canSwitchLanes() {
        return canSwitchLanes;
    }

    public int getRadius() {
        return radius;
    }
}
