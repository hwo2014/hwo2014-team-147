package noobbot;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.houston.slot_car.Car;
import com.houston.slot_car.message.Join;
import com.houston.slot_car.message.JoinSpecificTrack;
import com.houston.slot_car.message.MsgWrapper;
import com.houston.slot_car.message.Ping;
import com.houston.slot_car.message.SendMsg;
import com.houston.slot_car.message.from_server.CarPositions;
import com.houston.slot_car.racer.KeimolaRacer;
import com.houston.slot_car.racer.Racer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {

    String trackId;
    Racer racer;
    Car car;

//    private static final String TRACK_NAME = "keimola";
//    private static final String TRACK_NAME = "germany";
    private static final String TRACK_NAME = "usa";

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        if (TRACK_NAME == null) {
            new Main(reader, writer, new Join(botName, botKey));
        } else {
            new Main(reader, writer, new JoinSpecificTrack(botName, botKey, TRACK_NAME, 1));
        }
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        car = null;
        this.writer = writer;
        String line;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

            Map<String, Object> map = new HashMap<String, Object>();
            map = (Map<String, Object>)gson.fromJson(line, map.getClass());

            if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                car = Car.getCar((LinkedTreeMap)map.get("data"));
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                raceInit(map);
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("carPositions")) {
                send(racer.getAction(gson.fromJson(line, CarPositions.class)));
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("CRASH!");
                racer.printCrashDetails();
                send(new Ping());
            } else if (msgFromServer.msgType.equals("spawn")) {
                System.out.println("spawned");
                System.out.println(line);
                send(new Ping());
            } else if (msgFromServer.msgType.equals("lapFinished")) {
                System.out.println(line);
                send(new Ping());
            } else if (msgFromServer.msgType.equals("finish")) {
                System.out.println("FINISH!");
                System.out.println(line);
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                System.out.println(line);
            } else if (msgFromServer.msgType.equals("tournamentEnd")) {
                System.out.println("The end.");
            } else {
                System.out.println("UNKNOWN MESSAGE: " + line);
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    private void raceInit(Map<String, Object> map) {
        if (map.get("data") instanceof LinkedTreeMap) {
            LinkedTreeMap<String, Object> data = (LinkedTreeMap<String, Object>)map.get("data");
            if (data.get("race") instanceof LinkedTreeMap) {
                LinkedTreeMap<String, Object> race = (LinkedTreeMap<String, Object>)data.get("race");
                LinkedTreeMap<String, Object> trackMap = (LinkedTreeMap)race.get("track");

                trackId = trackMap.get("id").toString();
                racer = new KeimolaRacer();

                racer.buildTrack((ArrayList)trackMap.get("pieces"));
                racer.setLanes((ArrayList)trackMap.get("lanes"));
                racer.setMyCar(car);
            }
        } else {
            System.out.println("raceInit() map.get(\"data\").getClass().getName(): " + map.get("data").getClass().getName());
        }
    }

}
